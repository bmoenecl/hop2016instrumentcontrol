function out = write_awg(data, varargin)
%WRITE_AWG Write data to awg
%   syntax:
%   write_awg(data)
%   write_awg(data, 'PropertyName', PropertyValue,...)
%
%   arguments:
%   data: vector of length L
%   properties:
%   markers: L x 2 matrix of booleans (optional)
%   clock_freq: sample clock frequency (optional)
%   filename: write to this file on pc
%   filename_awg: write to this file on awg
%   address: GPIB address
%   data_to_file: convert data to file
%   file_to_awg: send file to awg

p = inputParser;
p.addOptional('markers', false(length(data), 2), @(x) size(x) == [2, length(data)] && all(islogical(x)));
p.addOptional('clock_freq', 200e6, @(x) x <= 200e6);
p.addOptional('filename', tempname);
p.addOptional('filename_awg', 'data_from_matlab.wfm');
p.addOptional('address', 'GPIB0::1::INSTR');
p.addOptional('data_to_file', true);
p.addOptional('file_to_awg', true);
p.parse(varargin{:});

if p.Results.data_to_file
    if ~iscolumn(data)
        data = data.';
    end
    BuildTektronixAWG710xWFM(data, p.Results.markers, p.Results.clock_freq, p.Results.filename);
end

if p.Results.file_to_awg
    out = python('../python/write_awg.py', p.Results.address, p.Results.filename, p.Results.filename_awg);
end

end

