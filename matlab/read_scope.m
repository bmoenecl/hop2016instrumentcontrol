function out = read_scope(varargin)
%READ_SCOPE Read data from scope
%   syntax:
%   out = read_scope('PropertyName', PropertyData,...)
%
%   properties:
%   filename: write to this file on pc
%   address: GPIB address
%   scope_to_file: read data from scope into file
%   file_to_data: read data from file

p = inputParser;
p.addOptional('filename', tempname);
p.addOptional('address', 'GPIB0::7::INSTR');
p.addOptional('scope_to_file', true);
p.addOptional('file_to_data', true);
p.parse(varargin{:});

if p.Results.scope_to_file
    out = python('../python/read_scope.py', p.Results.address, p.Results.filename);
end

if p.Results.file_to_data
    f = fopen(p.Results.filename', 'r');
    out = fread(f);
    fclose(f);
end





end

