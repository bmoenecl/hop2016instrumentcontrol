'''
Created on Apr 22, 2016

@author: bmoenecl
'''

import visa
import argparse


if __name__ == '__main__':
    p = argparse.ArgumentParser('write data to AWG')
    p.add_argument('address', metavar='ADDR', help='GPIB address')
    p.add_argument('filename_local', metavar='FILE', help='data file to be sent to awg')
    p.add_argument('filename_awg', metavar='FILE', help='destination path of file on awg')
    
    args = p.parse_args()
    
    # read file into bytes "data"
    data = b''
    with open(args.filename_local, 'rb') as f:
        byte = f.read(1)
        while byte:
            #do stuff
            data = data + byte
            byte = f.read(1)
            
    # convert data to definite length arbitrary block: 
    data_bytes = str(len(data))
    length_bytes = str(len(data_bytes))
    
    header = '#' + length_bytes + data_bytes
    total_data = header.encode('utf-8') + data
    
    # connect with awg over GPIB
    rm = visa.ResourceManager()
    awg = rm.open_resource(args.address)
    
    # send data
    msg = ('MMEM:DATA "' + args.filename_awg + '",').encode('utf-8') + total_data + '\n'.encode('utf-8')
    awg.write_raw(msg)
    
    
    

