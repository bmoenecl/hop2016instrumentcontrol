'''
Created on Apr 22, 2016

@author: bmoenecl
'''

import visa
import argparse


if __name__ == '__main__':
    p = argparse.ArgumentParser('write data to AWG')
    p.add_argument('address', metavar='ADDR', help='GPIB address')
    p.add_argument('filename_local', metavar='FILE', help='data file to s')
    
    args = p.parse_args()
    
    # connect with scope over GPIB
    rm = visa.ResourceManager()
    scope = rm.open_resource(args.address)
    
    # query data
    data = scope.query_binary_values(':WAV:DATA?', datatype='s', is_big_endian=False)
    
    # write bytes to file
    with open(args.filename_local, 'wb') as f:
        f.write(data[0])
    
    
    

