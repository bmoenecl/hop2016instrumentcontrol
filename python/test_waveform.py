from waveform import Waveform
import numpy as np
import math
import visa
from pyvisa.util import to_ieee_block

L=int(math.pow(2,12))
N=1024/4
f_awg = 200e6
data = [math.sin(x) for x in np.arange(0, N*2*math.pi, N*2*math.pi/L)]
markers = [(False, False)]*L

wfm = Waveform(data, markers, f_awg)
data = wfm.export()

data_bytes = str(len(data))
length_bytes = str(len(data_bytes))

header = '#' + length_bytes + data_bytes
total_data = header.encode('utf-8') + data

rm = visa.ResourceManager()
awg = rm.open_resource('GPIB0::1::INSTR')
#awg.timeout = 25000

print(data)

message = 'MMEM:DATA "testnew5.wfm",'.encode('utf-8') + total_data + '\n'.encode('utf-8')
print(awg.write_raw(message))

#data_read = awg.query_binary_values('MMEM:DATA? "testnew5.wfm"', datatype='s')[0]

#print(data_read)
