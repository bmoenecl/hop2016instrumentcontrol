from array import array
from functools import reduce
class Waveform:
    '''
    data: list of floats of length L
    markers: L-length lists of 2-length tuples of booleans
    clock_freq: float
    '''
    def __init__(self, data, markers, clock_freq):
        assert(len(data) == len(markers))
        assert(all(type(x) is float for x in data))
        assert(type(clock_freq) is float)
        self.data = data
        self.markers = markers
        self.clock_freq = clock_freq

    def export(self):
        L = len(self.data)

        sample_bytes = str(L*5)
        length_bytes = str(len(sample_bytes))

        header = 'MAGIC 1000\r\n#' + length_bytes + sample_bytes

        data_array = array('f', self.data)
        data_bytes = data_array.tobytes()

        marker_bytes = bytes([int(m[0]) + 2* int(m[1]) for m in self.markers])

        #all_bytes = [data_bytes[4*l:4*(l+1)] + marker_bytes[l:l+1] for l in range(0,L)]
        all_bytes_tmp = [data_bytes[4*l:4*(l+1)] + marker_bytes[l:l+1] for l in range(0,L)]
        all_bytes = reduce(lambda x,y: x+y, all_bytes_tmp)
        
        clock_str = 'CLOCK %7e' % self.clock_freq
        
        total = header.encode('utf-8') + all_bytes + clock_str.encode('utf-8')
        
        return total

